import sys
sys.path.append("/home/adebayo/Adafruit_ADS1x15")
import pymongo
import serial
from Adafruit_ADS1x15 import ADS1x15
import sys

PORT = "/dev/ttyACM0"
BAUD = 57600
MONGODB_URI = 'mongodb://bayo:1234@ds035713.mongolab.com:35713/todolist'
USB = 1
I2C = 2
ser_init = False
i2c_init = False
gsr = []    
pga, sps = 0512, 8

def init_i2c():
    global adc          #establish which sensor we're using (Thanks BookHolders!)
    adc = ADS1x15(ic=0x01)
    i2c_init = True     #let every know we exist


def init_serial(port, br):
    
    global ser                  #The global vaiable ser...
    ser = serial.Serial(port)   #reads from the following port..
    ser.baudrate = br           #at the baudrate br


    if not ser.isOpen():  #if the serial port isn't open,
        ser.open()        #open it
    ser_init = True       #let everyone know we exist

    
def get_gsr(le_type):
    if le_type == USB:      #if our sensor is connected via usb
        if not ser_init:    #if the serial port isnt initialized
            init_serial(PORT, BAUD) #initialize it
        val = int(ser.readline())

    if le_type == I2C:    #if our sensor is an I2C sensor:
        if not i2c_init:  #if we haven't initialized yet
            init_i2c()    #initialize the sensor            
        val = adc.readADCSingleEnded(0, pga, sps)  #get the value from the i2c sensor (from adafruit)

    voltage = float(val) * (5.0 / 1023.0)
    gsr.append(voltage)
    return gsr
        


def init_gsr():
    global client
    client = pymongo.MongoClient(MONGODB_URI)
    
    global db
    db = client.get_default_database()
    
    global collection
    collection = db["gsr"]

    
def post_gsr():
    #entry = {"text": str(get_gsr())}
    #collection.insert(entry)
    #collection.update({"_id": "sensor_data"}, {"text": erg.append(str(get_gsr())) }, upsert = True)
    collection.update({"_id": "sensor_data"}, {"text": get_gsr()}, upsert = True)

#init_gsr()
get_gsr(USB)

while True:
    #post_gsr()
    print get_gsr(USB)
    print "Posted!"
    
print "yass"
#the last thing we do is close the serial port (if its open)
if ser.isOpen():
    ser.close()

print "Done!"

